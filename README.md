# Desafio Front-End Developer

Este é um desafio prático para front-end developers que desejam entrar para o nosso time.

O desafio consiste na criação de um aplicação web simples para cadastro de usuários (CRUD) com autenticação.

## Modelo de dados:

-   Nome (obrigatório)
-   Telefone (opcional)
-   Email (obrigatório e deve ser um email válido)
-   Login (obrigatório)
-   Senha (obrigatório)

## Pré-requisitos

-   Deve ser possível cadastrar, editar, listar e excluir os dados cadastrados;
-   Autenticação usando JWT;
-   A persistência dos dados deve ser no localStorage ou IndexedDB;
-   Obrigatório a utilização de ReactJS 16.x com redux e ES6 ou ES7;
-   Utilizar a biblioteca de componentes Bootstrap 4.
-   A página deve ser responsiva;
-   O projeto deve ser publicado em um repositório publico do Github/Bitbucket/Gitlab (crie uma se você não possuir).

## Desejável

-   Utilização de pré-processador CSS
-   Analise de código com ESlint
-   Utilizar o bundler Webpack
-   Melhore a qualidade do código estendendo algum style guide
-   Testes de unidade

## O que esperamos:

-   Utilização de boas práticas
-   Técnicas de clean code
-   Que o app suba apenas com o comando: yarn start

## Plus (Opcional)

Publicar a aplicação em algum serviço como heroku, firebase ou netlify. Todos são gratuitos para projetos pessoais.
